WebGL Experiment
================

Hi, this is a WebGL experiment. I intend it to eventually become a racing game. But first I will have to figure out how this thing works. The code should be fairly easy to understand, and I want to keep it that way.

I have tested it on Firefox and Chrome, where I use vendor-prefixed experimental APIs. I always use the standard method too, so the game should eventually work in any browser.

You can see a hosted version [here](http://remcokranenburg.com).


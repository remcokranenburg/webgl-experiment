/**
 * WindowManager - Provides window management functions like fullscreen and resize
 */

function WindowManager(renderer, camera, params, stats)
{
	this.isFullscreen = false;
	
	var thisWindowManager = this;
	var windowResized = true; // init to true to force resize at start
	
	// automatic performance tuning variables
	var tuning = {
		minWidth: 320,
		minHeight: 240,
		interval: 1000, // delay before changing resolution
		minFps: 35, // if lower, decrease resolution
		maxFps: 45, // if higher, increase resolution
		decResolution: false,
		incResolution: false,
		timerRunning: false,
		timer: 0 // set to Date.now() + interval when FPS bounds are exceeded
	}
	
	// fullscreen change callbacks
	var onEnterFullscreen = params.onEnterFullscreen;
	var onExitFullscreen = params.onExitFullscreen;
	
	window.addEventListener('resize', function() { windowResized = true; });
	
	// on fullscreenchange
	document.addEventListener('fullscreenchange', onFullscreenChange);
	document.addEventListener('mozfullscreenchange', onFullscreenChange);
	document.addEventListener('webkitfullscreenchange', onFullscreenChange);
	
	/**
	 * Toggles between normal 1:1 resolution and automatic tuning of resolution depending on FPS.
	 */
	
	this.toggleAutomaticTuning = function(e)
	{
		windowResized = true;
	}
	
	/**
	 * Perform a canvas resize if window was resized. Should be called just before a new frame is drawn (i.e. in the
	 * drawing loop) because it destroys the buffer. If it is called directly on resize, the canvas will flicker.
	 */
	
	this.maybeResize = function()
	{
		var width = window.innerWidth;
		var height = window.innerHeight;
		var targetWidth = renderer.context.canvas.width;
		var targetHeight = renderer.context.canvas.height;
		
		if(windowResized)
		{
			targetWidth = width;
			targetHeight = height;
		}
		else if(doc.q('#tuning').checked)
		{
			if(!tuning.timerRunning)
			{
				if(stats.getFps() < tuning.minFps)
				{
					tuning.timerRunning = true;
					tuning.decResolution = true;
					tuning.timer = Date.now() + tuning.interval;
				}
				else if(stats.getFps() > tuning.maxFps)
				{
					tuning.timerRunning = true;
					tuning.incResolution = true;
					tuning.timer = Date.now() + tuning.interval;
				}
			}
			else
			{
				if(tuning.timer > Date.now())
				{
					if(stats.getFps() >= tuning.minFps && stats.getFps() <= tuning.maxFps)
					{
						tuning.timerRunning = false;
						tuning.incResolution = false;
						tuning.decResolution = false;
						tuning.timer = 0;
					}
				}
				else if(tuning.decResolution)
				{
					// decrease target width and height
					targetWidth = Math.max(tuning.minWidth, Math.floor(0.9 * targetWidth));
					targetHeight = Math.max(tuning.minHeight, Math.floor(0.9 * targetHeight));
					
					tuning.timerRunning = false;
					tuning.incResolution = false;
					tuning.decResolution = false;
					tuning.timer = 0;
				}
				else if(tuning.incResolution)
				{
					// increase target width and height
					targetWidth = Math.min(width, Math.floor(1.1 * targetWidth));
					targetHeight = Math.min(height, Math.floor(1.1 * targetHeight));
					
					tuning.timerRunning = false;
					tuning.incResolution = false;
					tuning.decResolution = false;
					tuning.timer = 0;
				}
			}	
		}
		
		if(targetWidth != renderer.context.canvas.width || targetHeight != renderer.context.canvas.height)
		{
			console.log('resized', targetWidth, targetHeight);
			renderer.setSize(targetWidth, targetHeight);
			camera.aspect = width / height;
			camera.updateProjectionMatrix();
		}
		
		windowResized = false;
	}
	
	/**
	 * Requests the browser to turn the body element into a fullscreen element
	 */
	
	this.requestFullscreen = function(e)
	{
		if(!thisWindowManager.isFullscreen)
		{
			console.log('request enter fullscreen')
			if(document.body.requestFullscreen) document.body.requestFullscreen();
			else if(document.body.mozRequestFullScreen) document.body.mozRequestFullScreen();
			else if(document.body.webkitRequestFullscreen) document.body.webkitRequestFullscreen();
		}
		else
		{
			console.log('request exit fullscreen')
			if(document.exitFullscreen) document.exitFullscreen();
			else if(document.mozCancelFullScreen) document.mozCancelFullScreen();
			else if(document.webkitExitFullscreen) document.webkitExitFullscreen();
		}
	}
	
	/**
	 * Private method that performs the enter function and requests pointer lock on entering fullscreen, and performs
	 * the exit function on exiting fullscreen.
	 */
	
	function onFullscreenChange()
	{
		if(!thisWindowManager.isFullscreen)
		{
			console.log('enter fullscreen');
			if(onEnterFullscreen) onEnterFullscreen();
			thisWindowManager.isFullscreen = true;
			
			// request pointer lock
			(	document.body.requestPointerLock ||
				document.body.mozRequestPointerLock ||
				document.body.webkitRequestPointerLock ).call(document.body);
		}
		else
		{
			console.log('exit fullscreen');
			if(onExitFullscreen) onExitFullscreen();
			thisWindowManager.isFullscreen = false;
		}
	}
}

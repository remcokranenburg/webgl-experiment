/**
 * Create ground
 */

function Ground(size)
{
	var geometry = new THREE.PlaneGeometry(size, size, 50, 50);
	
	geometry.vertices.forEach(function(vertex) {
		vertex.z = Math.random() * 0.1;
	});
	
	geometry.verticesNeedUpdate = true;
	geometry.computeFaceNormals();
	geometry.computeVertexNormals();
	
	var material = new THREE.MeshLambertMaterial({
		color: new THREE.Color(0, 1, 0)
	});
	
	// call super constructor
	THREE.Mesh.call(this, geometry, material);
	
	this.receiveShadow = true;
	this.castShadow = true;
	
	var physicsShape = new CANNON.Plane();
	this.body = new CANNON.RigidBody(0, physicsShape, physicsMaterial);
	this.body.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);
	this.body.quaternion.copy(this.quaternion);
}

/**
 * Create Ground prototype from super class THREE.Mesh prototype
 */

Ground.prototype = Object.create(THREE.Mesh.prototype);



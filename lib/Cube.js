/**
 * Cube mesh with box geometry and 6 randomly colored faces
 */

function Cube(size)
{
	var geometry = null;
	var material = null;
	
	this.materials = [];
	this.newColors = [];
	
	for(var i = 0; i < 6; i++)
	{
		var color = Math.floor(Math.random() * 0xffffff);
		this.materials.push(new THREE.MeshLambertMaterial({ color: color }));
		this.newColors.push(new THREE.Color(Math.floor(Math.random() * 0xffffff)));
	}

	geometry = new THREE.BoxGeometry(size, size, size);
	material = new THREE.MeshFaceMaterial(this.materials);
	
	// call super constructor
	THREE.Mesh.call(this, geometry, material);
	
	this.castShadow = true;
	
	// physics
	var shape = new CANNON.Box(new CANNON.Vec3(size / 2, size / 2, size / 2));
	this.body = new CANNON.RigidBody(0.7, shape, physicsMaterial);
}

/**
 * Create Cube prototype from super class THREE.Mesh prototype
 */

Cube.prototype = Object.create(THREE.Mesh.prototype);

/**
 * Calculates new position and random colors for next frame
 */

Cube.prototype.animate = function(delta)
{
	this.newColor = new THREE.Color(Math.floor(Math.random() * 0xffffff));
	
	for(var i = 0; i < this.materials.length; i++)
	{
		var color = this.materials[i].color;
		var newColor = this.newColors[i];
		
		if(color.equals(newColor))
		{
			this.newColors[i] = new THREE.Color(Math.floor(Math.random() * 0xffffff));
		}
		else
		{
			if(color.r - newColor.r < 0) color.r = Math.min(newColor.r, color.r + 0.0001 * delta);
			if(color.r - newColor.r > 0) color.r = Math.max(newColor.r, color.r - 0.0001 * delta);
			if(color.g - newColor.g < 0) color.g = Math.min(newColor.g, color.g + 0.0001 * delta);
			if(color.g - newColor.g > 0) color.g = Math.max(newColor.g, color.g - 0.0001 * delta);
			if(color.b - newColor.b < 0) color.b = Math.min(newColor.b, color.b + 0.0001 * delta);
			if(color.b - newColor.b > 0) color.b = Math.max(newColor.b, color.b - 0.0001 * delta);
		}
	}
}

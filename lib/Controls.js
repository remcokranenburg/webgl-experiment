/**
 * Controls - Turns user input into actions such as camera movement or going fullscreen
 */

function Controls(canvas, windowManager)
{
	this.windowManager = windowManager;
	this.mouseSpeed = 0.1;
	
	var move = {
		mouseDown: false,
		forward: false,
		backward: false,
		left: false,
		right: false,
		oForward: false,
		oBackward: false,
		oLeft: false,
		oRight: false,
	};
	
	// add event listeners
	document.addEventListener('keydown', handleKeyDown);
	document.addEventListener('keyup', handleKeyUp);
	canvas.addEventListener('mousedown', function() { move.mouseDown = true; });
	canvas.addEventListener('mouseup', function() { move.mouseDown = false; });

	function handleKeyDown(e)
	{
		var passthrough = false;
		
		switch(String.fromCharCode(e.keyCode))
		{
			case 'W':
			move.forward = true;
			break;
		
			case 'S':
			move.backward = true;
			break;
		
			case 'A':
			move.left = true;
			break;
		
			case 'D':
			move.right = true;
			break;
			
			default:
			switch(e.keyCode)
			{
				case 122: // F11
				windowManager.requestFullscreen(e);
				break;
				
				case 38: // Up
				move.oForward = true;
				break;
				
				case 40: // Down
				move.oBackward = true;
				break;
				
				case 37: // Left
				move.oLeft = true;
				break;
				
				case 39: // Right 
				move.oRight = true;
				break;
				
				default:
				console.log("key pressed", e.keyCode)
				passthrough = true;
				break;
			}
		}
		
		if(!passthrough)
		{
			e.preventDefault();
		}
	}

	function handleKeyUp(e)
	{
		switch(String.fromCharCode(e.keyCode))
		{
			case 'W':
			move.forward = false;
			break;
		
			case 'S':
			move.backward = false;
			break;
		
			case 'A':
			move.left = false;
			break;
		
			case 'D':
			move.right = false;
			break;
		
			default:
			switch(e.keyCode)
			{
				case 38: // Up
				move.oForward = false;
				break;
				
				case 40: // Down
				move.oBackward = false;
				break;
				
				case 37: // Left
				move.oLeft = false;
				break;
				
				case 39: // Right 
				move.oRight = false;
				break;
				
				default:
				break;
			}
			break;
		}
	}

  this.moveObject = function(camera, object, delta)
  {
    var forceFactor = 0.5;
    
    if(move.oForward)
    {
      var direction = new THREE.Vector3(0, 0, -1);
      direction.applyQuaternion(camera.quaternion);
      direction.multiplyScalar(forceFactor);
      var force = new CANNON.Vec3(direction.x, 0, direction.z);
      object.applyImpulse(force, object.position);
    }
    
    if(move.oBackward)
    {
      var direction = new THREE.Vector3(0, 0, -1);
      direction.applyQuaternion(camera.quaternion);
      direction.multiplyScalar(-1 * forceFactor);
      var force = new CANNON.Vec3(direction.x, 0, direction.z);
      object.applyImpulse(force, object.position);
    }
    
    if(move.oLeft)
    {
      var direction = new THREE.Vector3(0, 0, -1);
      direction.applyQuaternion(camera.quaternion);
      direction.normalize();
      direction.y = 0;
      direction.applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI / 2);
      direction.multiplyScalar(forceFactor);
      var force = new CANNON.Vec3(direction.x, 0, direction.z);
      object.applyImpulse(force, object.position);
    }
    
    if(move.oRight)
    {
      var direction = new THREE.Vector3(0, 0, -1);
      direction.applyQuaternion(camera.quaternion);
      direction.normalize();
      direction.y = 0;
      direction.applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI / -2);
      direction.multiplyScalar(forceFactor);
      var force = new CANNON.Vec3(direction.x, 0, direction.z);
      object.applyImpulse(force, object.position);
    }
  }  
  
	this.moveCamera = function(camera, position, delta)
	{
		var speed = 0.002;
		
		if(move.forward)
		{
			camera.translateZ(-speed * delta);
		}
		
		if(move.backward)
		{
			camera.translateZ(speed * delta);
		}
		
		if(move.left)
		{
			camera.translateX(-speed * delta);
		}
		
		if(move.right)
		{
			camera.translateX(speed * delta);
		}
		
		camera.lookAt(position);
	}
}
